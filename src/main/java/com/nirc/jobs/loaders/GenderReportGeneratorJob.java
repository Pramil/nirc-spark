package com.nirc.jobs.loaders;

import com.nirc.engine.utilities.DataFrameUtils;
import com.nirc.jobs.base.BaseLoader;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;

import static org.apache.spark.sql.SaveMode.Overwrite;
import static org.apache.spark.sql.functions.lit;

/**
 * Created by patthar on 3/20/17.
 */
public class GenderReportGeneratorJob extends BaseLoader {

    @Override
    public String processJob(JavaSparkContext sparkContext) {
        /**
         * Specification of the report is:
         * source fields :
         id
         version
         age
         belief_id
         cast_id
         country_id
         disability_id
         election_area_id
         family_id
         flexibility
         gender_id
         is_head_of_the_family
         lifestyle_id
         name
         occupation_id
         palika_id
         phone_number
         religion_id
         visibility
         ward_id
         influence
         take_voter_id
         *
        * */

        /**
         * Destination fields :
         * id
         version
         count
         election_area_id
         gender_id
         palika_id
         political_belief_i
         ward_id
         *
         * */

        sqlContext = new SQLContext(sparkContext);
        //reading data from sql table
        DataFrame memberTable = DataFrameUtils.loadTableDataFrame("localhost","member",sqlContext);

        //job specification
        memberTable= memberTable.groupBy("election_area_id","gender_id","palika_id","ward_id").count().alias("count");

        //checking schema
        memberTable.printSchema();

        //appending unique data
        memberTable = DataFrameUtils.appendUniqueId(memberTable,"id",sqlContext);

        //adding literal data
        memberTable = memberTable.withColumn("version",lit("0"));

        //writing to the table
        DataFrameUtils.saveToTable(Overwrite,memberTable,"group_test_data","localhost");

        //now writing in its own new table
        return "JOB HAS BEEN SUCCESFULLY COMPILED";
    }
}
