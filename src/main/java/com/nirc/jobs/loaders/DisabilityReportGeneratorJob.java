package com.nirc.jobs.loaders;

import com.nirc.engine.utilities.DataFrameUtils;
import com.nirc.jobs.base.BaseLoader;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;

import static org.apache.spark.sql.SaveMode.Overwrite;
import static org.apache.spark.sql.functions.lit;

/**
 * Created by patthar on 3/20/17.
 */
public class DisabilityReportGeneratorJob extends BaseLoader {

    @Override
    public String processJob(JavaSparkContext sparkContext) {

        sqlContext = new SQLContext(sparkContext);
        //reading data from sql table
        DataFrame disabilityDF = DataFrameUtils.loadTableDataFrame("localhost","member",sqlContext);

        //job specification
        disabilityDF= disabilityDF.groupBy("election_area_id","gender_id","palika_id","ward_id").count().alias("count");

        //checking schema
        disabilityDF.printSchema();

        //appending unique data
        disabilityDF = DataFrameUtils.appendUniqueId(disabilityDF,"id",sqlContext);

        //adding literal data
        disabilityDF = disabilityDF.withColumn("version",lit("0"));

        //writing to the table
        DataFrameUtils.saveToTable(Overwrite,disabilityDF,"group_test_data","localhost");

        //now writing in its own new table
        return "JOB HAS BEEN SUCCESFULLY COMPILED";
    }
}
