package com.nirc.jobs.loaders;

import com.nirc.engine.utilities.DataFrameUtils;
import com.nirc.jobs.base.BaseLoader;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;

import static org.apache.spark.sql.SaveMode.Overwrite;

/**
 * Created by patthar on 3/20/17.
 */
public class TestReportGeneratorJob extends BaseLoader {

    @Override
    public String processJob(JavaSparkContext sparkContext) {
        sqlContext = new SQLContext(sparkContext);
        DataFrame memberTable = DataFrameUtils.loadTableDataFrame("localhost","test",sqlContext);
        memberTable= memberTable.filter("first_name <> 'Pramil'");
        DataFrameUtils.saveToTable(Overwrite,memberTable,"first_test","localhost");
        //now writing in its own new table
        return "JOB HAS BEEN SUCCESFULLY COMPILED";
    }
}
