package com.nirc.jobs.base;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
  Created by ppaudel.
 */

/**
 * This is template for Child
 * It also makes all child serializable
 *For Data processing Spark SQL, Spark DataFrames and Spark Java RDD are used
 */

public abstract class BaseLoader implements SparkInterface, Serializable {

    protected  String result = null;
    protected SQLContext sqlContext;
    protected String config;


    public void setConfig(String config) {
        this.config = config;
    }

    /**
     * Note: This is Just an Template. All child should override it as required
     */
    @Override
    public abstract String processJob(JavaSparkContext sparkContext);

    protected String jobSuccess() {
        result = "Success";
        return result;
    }
    @Override
    public void configure(SparkConf conf) {
        conf.setAppName("Spark Engine");
    }

//    protected DataFrame getDerivedDF(DataFrame dataFrame, List<String> additionFields, Function<Row, Row> function) {
//        List<String> allFields = new ArrayList<>(Arrays.asList(dataFrame.schema().fieldNames()));
//        if (additionFields!=null)
//            allFields.addAll(additionFields);
//        return sqlContext.createDataFrame(
//                dataFrame.javaRDD().map(function),
//                getSchema(allFields));
//    }
//
//    public void setUpForS3(JavaSparkContext sparkContext) {
//        sparkContext.hadoopConfiguration().set("fs.s3n.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem");
//        sparkContext.hadoopConfiguration().set("fs.s3.impl", "org.apache.hadoop.fs.s3.S3FileSystem");
//    }
}
