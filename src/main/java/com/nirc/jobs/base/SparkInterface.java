package com.nirc.jobs.base;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

/**
 * Created by patthar on 3/20/17.
 */
public interface SparkInterface {
    String processJob(JavaSparkContext sparkContext);
    void configure(SparkConf conf);
}
