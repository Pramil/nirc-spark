package com.nirc.engine.options;

import org.apache.commons.cli.*;

import java.io.Serializable;

/**
 * Created by topsykretts on 6/9/16.
 */
public class NircOptions implements Serializable {

    public Option job;
    public Option index;
    public Option config;
    protected Options options;

    public NircOptions() {
        initializeOptions();
    }

    protected void initializeOptions() {
        this.job = addOptionWithArg("job", "Name of Job to run");
        this.index = addOptionWithArg("index", "Name of Elastic Search index");
        this.config = addOptionWithArg("config", "Name of config file");
        options = new Options();
        options.addOption(job);
        options.addOption(index);
        options.addOption(config);
    }

    private Option addOption(String option, String description) {

        OptionBuilder.withArgName(option);
        OptionBuilder.withDescription(description);
        return OptionBuilder.create(option);
    }

    private Option addOptionWithArg(String option, String description) {

        OptionBuilder.withArgName(option);
        OptionBuilder.withDescription(description);
        OptionBuilder.hasArg();
        return OptionBuilder.create(option);
    }

    public CommandLine parse(String[] args) {
        try {
            GnuParser parser = new GnuParser();
            return parser.parse(options, args);
        } catch (ParseException pe) {
            System.out.println(pe);
            return (null);
        }
    }
}
