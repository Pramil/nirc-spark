package com.nirc.engine;

import com.nirc.engine.options.NircOptions;
import com.nirc.jobs.base.BaseLoader;
import com.nirc.engine.utilities.ResourceUtils;
import org.apache.commons.cli.CommandLine;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Date;

/**
 * Created by patthar on 3/18/17.
 */
public class NircSpark {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        args = new String[]{"-job", "GenderReport", "-config", "gender"};
        SparkConf conf = new SparkConf();
        //conf.setMaster("spark://Patthar-PC:7077");
        conf.setMaster("local[4]");
                 //.setAppName("ClusterScore")
                 // .setMaster("spark://172.1.1.1:7077") ;
                 //  .set("spark.storage.memoryFraction", "1");


        NircOptions options = new NircOptions();
        CommandLine commandLine = options.parse(args);
        System.out.println("commandLine = " + commandLine);
        String jobName = commandLine.getOptionValue("job");
        Class clazz = Class.forName("com.nirc.jobs.loaders." + ResourceUtils.getJobClassName(jobName.trim()));
        BaseLoader job = (BaseLoader) clazz.newInstance();
        //job.setIndex(commandLine.getOptionValue("index"));
        job.setConfig(commandLine.getOptionValue("config")+".properties");
        job.configure(conf);
        JavaSparkContext sparkContext = new JavaSparkContext(conf);
        //sparkContext.setMaster()
        sparkContext.setLogLevel("WARN");
        //job.setUpForS3(sparkContext);
        Date start = new Date();
        String ack = job.processJob(sparkContext);
        Date end = new Date();
        System.out.println("Started job "+jobName +" at "+ start);
        System.out.println("Finished job "+jobName +" at "+end);
        long diff = (end.getTime()-start.getTime());
        System.out.println("Job execution took "+diff/1000+" sec --- " +(diff/(1000*60))+" min");
        System.out.println("Message form job = "+ ack);
        sparkContext.stop();
    }
}
