package com.nirc.engine.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by topsykretts on 6/9/16.
 */
public class ResourceUtils {

    public static String getJobClassName(String jobName) {
        Properties loaderProperties = new Properties();
        InputStream stream = ResourceUtils.class.getClassLoader().getResourceAsStream("loaders/loadersLookup.properties");
        try {
            loaderProperties.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not find file loaders/loadersLookup.properties "+e.getLocalizedMessage());
        }
        return loaderProperties.get(jobName).toString().trim();
    }

    public static Properties getJobConfigProperties(String fileName) {
        return getConfigProperties("jobconfig/"+fileName);
    }
    public static Properties getConfigProperties(String fullPath) {
        Properties configProperties = new Properties();
        InputStream stream = ResourceUtils.class.getClassLoader().getResourceAsStream(fullPath);
        try {
            configProperties.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not find "+ fullPath+"\n"+e.getLocalizedMessage());
        }
        return configProperties;
    }
}
