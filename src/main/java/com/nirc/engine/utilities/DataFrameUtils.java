package com.nirc.engine.utilities;

import com.nirc.engine.function.UniqueIdAppenderFunction;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.*;

import static org.apache.spark.sql.functions.col;

/**
 * Created by topsykretts on 6/9/16.
 */
public class DataFrameUtils {

    public static StructType insertColInSchema(StructType oldSchema, String colName, DataType dataType) {
        List<StructField> newFields = new ArrayList<>(Arrays.asList(oldSchema.fields()));
        newFields.add(DataTypes.createStructField(colName,dataType,true));
        return DataTypes.createStructType(newFields);
    }
    public static StructType insertColInSchema(StructType oldSchema, String colName) {
        return insertColInSchema(oldSchema, colName, DataTypes.StringType);
    }

    public static StructType getSchema(List<String> fields) {
        List<StructField> addressStructFields = new ArrayList<StructField>();
        for (String field:fields ) {
            addressStructFields.add(DataTypes.createStructField(field,DataTypes.StringType,true));
        }

        return DataTypes.createStructType(addressStructFields);
    }

    public static JavaRDD<Row> getRowRDD(JavaRDD<String> rdd, final String delimiter) {
        return rdd.map((Function<String, Row>) s -> {
            if (delimiter == null)
                return RowFactory.create(s);
            String[] columns = s.split(delimiter,-1);
            List<String> tuples = new ArrayList<>();
            for (String col : columns) {
                tuples.add(col.trim());
            }
            return RowFactory.create(tuples.toArray());
        });
    }

    public static JavaRDD<Row> getRowRDD(JavaRDD<String> rdd, final String delimiter, boolean quoted) {
        if (!quoted) {
            return getRowRDD(rdd, delimiter);
        } else {
            return rdd.map((Function<String, Row>) s -> {
                if (delimiter == null)
                    return RowFactory.create(s);
                String[] columns = s.split(delimiter+"(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
                List<String> tuples = new ArrayList<>();
                for (String col : columns) {
                    if (col.startsWith("\"")) {
                        tuples.add(col.trim().substring(1,col.length()-1));
                    } else {
                        tuples.add(col.trim());
                    }
                }
                return RowFactory.create(tuples.toArray());
            });
        }
    }

//    public static DataFrame appendUniqueId(DataFrame dataFrame, String idFieldName, SQLContext sqlContext) {
//        //generating Id
//        JavaPairRDD<Row, Long> combinedPairRDD = dataFrame.javaRDD().zipWithUniqueId();
//        JavaRDD<Row> combinedRDD = combinedPairRDD.map(new UniqueIdAppenderFunction());
//        ArrayList<String> allFields = new ArrayList<String>(){{add(idFieldName);}};
//        allFields.addAll(Arrays.asList(dataFrame.schema().fieldNames()));
//        return sqlContext.createDataFrame(
//                combinedRDD,
//                getSchema(allFields));
//    }
//
//    public static JavaRDD<String> appendUniqueId(JavaRDD<String> inputRDD,String delimiter) {
//        return inputRDD.zipWithUniqueId().map(new Function<Tuple2<String,Long>,String>() {
//            @Override
//            public String call(Tuple2<String, Long> pair) throws Exception {
//                return pair._2()+(StringUtils.isNotNull(delimiter)?delimiter:"|")+pair._1();
//            }
//        });
//    }
    public static Column[] getColsFromList(List<String> fields) {
        List<Column> cols = new LinkedList<>();
        for (String field: fields) {
            cols.add(col(field));
        }
        return cols.toArray(new Column[1]);
    }

    public static DataFrame removeDuplicates(DataFrame dataFrame, String... ignoreFields) {
        List<String> allFields = new LinkedList<>(Arrays.asList(dataFrame.columns()));
        List<String> exceptFields = new LinkedList<>(Arrays.asList(ignoreFields));
        allFields.removeAll(exceptFields);
        return dataFrame.dropDuplicates(allFields.toArray(new String[1]));
    }

    public static DataFrame removeDuplicates(DataFrame dataFrame) {
        return removeDuplicates(dataFrame, "id");
    }

    public static DataFrame appendUniqueId(DataFrame dataFrame, String idFieldName, SQLContext sqlContext) {
        //generating Id
        JavaPairRDD<Row, Long> combinedPairRDD = dataFrame.javaRDD().zipWithUniqueId();
        JavaRDD<Row> combinedRDD = combinedPairRDD.map(new UniqueIdAppenderFunction());
        ArrayList<String> allFields = new ArrayList<String>(){{add(idFieldName);}};
        allFields.addAll(Arrays.asList(dataFrame.schema().fieldNames()));
        return sqlContext.createDataFrame(
                combinedRDD,
                getSchema(allFields));
    }

    public static DataFrame loadTableDataFrame(String hostName, String tableName,SQLContext sqlContext){
        Properties dbConfigProperty = ResourceUtils.getConfigProperties("config/database.properties");
        //System.out.println("dbConfigProperty.getProperty(\"localhost.url\") = " + dbConfigProperty.getProperty("localhost.url"));
            DataFrame rc = sqlContext.read().format("jdbc")
                    .option("user", dbConfigProperty.getProperty(hostName+ ".user"))
                    .option("password", dbConfigProperty.getProperty(hostName+ ".password"))
                    .option("url", dbConfigProperty.getProperty(hostName+ ".url"))
                    .option("driver", "com.mysql.jdbc.Driver")
                    .option("dbtable", tableName).load();
            return rc;
    }

    public static void saveToTable(SaveMode saveMode, DataFrame dataFrame, String tableName, String hostname)
    {
        Properties dbConfigProperty = ResourceUtils.getConfigProperties("config/database.properties");
        String url = dbConfigProperty.getProperty(hostname+ ".url");
        Properties p = new Properties();
        p.setProperty("user", dbConfigProperty.getProperty(hostname+ ".user"));
        p.setProperty("password", dbConfigProperty.getProperty(hostname+ ".password"));
        p.setProperty("driver", "com.mysql.jdbc.Driver");
        dataFrame.write().mode(saveMode).jdbc(url, tableName, p);
    }



    }
