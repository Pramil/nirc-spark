package com.nirc.engine.function;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ppaudel.
 */
public class UniqueIdAppenderFunction implements Function<Tuple2<Row, Long>, Row> {
    @Override
    public Row call(Tuple2<Row, Long> rowLongTuple2) throws Exception {
        Row row = rowLongTuple2._1();
        Long id = rowLongTuple2._2();
        List<String> tuples = new ArrayList<String>();
        tuples.add(String.valueOf(id));
        for (int i=0;i<row.size();i++) {
            tuples.add(String.valueOf(row.getLong(i)));
        }
        return RowFactory.create(tuples.toArray());

    }
}
